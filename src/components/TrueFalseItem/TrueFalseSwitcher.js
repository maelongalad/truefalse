import React, { Component, PropTypes } from 'react';

export default class TrueFalseSwitcher extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  componentWillMount() {
    this.setState({
      isSwitchedOn: this.props.isSwitchedOn
    })
  }

  render() {

    const {
      isSwitchedOn
    } = this.props;

    const elementClassName = "true-false-switcher " + (isSwitchedOn ? "turned-on" : "turned-off");

    return (
      <div
        className = { elementClassName }
        onClick = { this.switchTrueFalseItemLocal.bind(this) }
      >
        { isSwitchedOn ? "TRUE" : "FALSE" }
      </div>
    )
  }

  switchTrueFalseItemLocal(e) {
    if(typeof this.props.parentIndex != 'undefined') return this.props.switchTrueFalseItem(this.props.parentIndex);
    this.props.switchTrueFalseStatus()
  }
}

TrueFalseSwitcher.propTypes = {
  isSwitchedOn: PropTypes.bool.isRequired,
};
