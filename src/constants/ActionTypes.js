/**
 * Constants are important - they describe what type of action is performed
 * within your app. Combined with the DevTools/logger, you can see how state and subsequently
 * your UI is being affected.
 */
export const ADD_TRUE_FALSE_ITEM    = 'ADD_TRUE_FALSE_ITEM';
export const SWITCH_TRUE_FALSE_ITEM = 'SWITCH_TRUE_FALSE_ITEM';
